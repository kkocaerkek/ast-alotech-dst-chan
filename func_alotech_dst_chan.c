#include "asterisk.h"
ASTERISK_FILE_VERSION(__FILE__, "$Revision: $")
#include "asterisk/module.h"
#include "asterisk/pbx.h"
#include "asterisk/channel.h"

#ifndef AST_MODULE
#define AST_MODULE "func_alotech_dst_chan"
#endif

/*
struct find_by_name_args {
    const char *name;
    size_t len;
};
static int find_by_name_cb(void *obj, void *arg, void *data, int flags)
{
    struct ast_channel *target = obj;
    struct find_by_name_args *args = data;
    ast_channel_lock(target);
    if (!strncasecmp(target->name, args->name, args->len)) {
        return CMP_MATCH | CMP_STOP;
    }
    ast_channel_unlock(target);

    return 0;
}

static struct ast_channel *my_ast_get_channel_by_name_locked(const char *channame)
{
    char *chkchan;
    struct find_by_name_args find_args;

    if (strchr(channame, '-')) {
        find_args.len = strlen(channame);
        find_args.name = channame;
    } else {
        find_args.len = strlen(channame) + 1;
        chkchan = ast_alloca(find_args.len + 1);
        strcpy(chkchan, channame);
        strcat(chkchan, "-");
        find_args.name = chkchan;
    }

    return ast_channel_callback(find_by_name_cb, NULL, &find_args, 0);
}
*/
static int acf_dstch_exec(struct ast_channel *chan, const char *func,
             char *param, char *buffer, size_t buflen)
{
    struct ast_channel *c = NULL;
    
    //c = my_ast_get_channel_by_name_locked(param);
    c = ast_channel_get_by_name(param);
    if (c) {
        ast_channel_lock(c);
        if(c->cdr){
            //ast_log(LOG_NOTICE, "Destination channel : %s\n", c->cdr->dstchannel);
            snprintf(buffer, buflen, c->cdr->dstchannel);
        }
        ast_channel_unlock(c); 
        c = ast_channel_unref(c);
    }else{
        ast_log(LOG_WARNING, "Destination channel not found :(\n");
    }
    return 0;
}


static struct ast_custom_function acf_dstch = {
    .name = "ALOTECH_DST_CHANNEL",
    .synopsis = "Gets destination channel name of provided channel."
    " If provided channel does not have destination returns empty string",
    .syntax = "ALOTECH_DST_CHANNEL(channel_name)",
    .arguments =
    "  channel_name       - Name of the origin channel\n",
    .read = acf_dstch_exec
};

static int load_module(void)
{
    return ast_custom_function_register(&acf_dstch);
}

static int unload_module(void)
{
    ast_custom_function_unregister(&acf_dstch);
    return 0;
}

AST_MODULE_INFO_STANDARD(ASTERISK_GPL_KEY, "");


